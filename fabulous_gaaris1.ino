#include <Servo.h>

const int potPin = A0; // Pin connected to potentiometer
const int servoPin = 9; // Pin connected to servo signal

Servo myServo; // Create a servo object

void setup() {
  myServo.attach(servoPin); // Attach the servo to the specified pin
}

void loop() {
  int potValue = analogRead(potPin); // Read the potentiometer value (0-1023)
  int servoAngle = map(potValue, 0, 1023, 0, 180); // Map the value to an angle (0-180 degrees)
  myServo.write(servoAngle); // Set the servo to the corresponding angle
  delay(15); // Small delay for the servo to reach the position
}
